import { LitElement, html, css } from "lit-element";
import { Router } from "@vaadin/router";
import "./views/index.js";
import "./views/signup.js";
import "./views/terms-conditions.js";
import { sharedStyles } from "./css/shared.js";

/**
 * @class TestRepo
 * @extends LitElement
 * @desc TestRepo custom element
 */
export class TestRepo extends LitElement {
  /**
   * @desc Properties for the TestRepo class
   * @return {Object}
   */
  static get properties() {
    return {
      router: { type: Object },
    };
  }

  /**
   * @desc Styles for the TestRepo class
   * @return {Array}
   */
  static get styles() {
    return [
      sharedStyles,
      css`
        :host {
          display: block;
          max-width: 1140px;
          margin: 0 auto;
          padding: 3rem 1rem;
        }

        header {
          display: flex;
          align-items: center;
          justify-content: space-between;
        }

        main {
          padding: 4rem 0;
        }

        header ul {
          display: flex;
          align-items: center;
          list-style: none;
          padding: 0;
        }

        header ul li {
          display: inline-block;
          color: var(--primary-color);
        }

        a {
          cursor: pointer;
        }

        nav a {
          text-decoration: none;
        }

        header ul li:not(:last-child) {
          margin-right: 1rem;
        }
      `,
    ];
  }

  /** @desc Renders a TemplateResult (html``) to the page
   * @return {TemplateResult}
   */
  render() {
    return html`
      <header>
        <a href="/" title="View Test Repo Home">
          <img
            src="../img/ertk-logo.png"
            title="Test Repo"
            alt="Test Repo Logo"
          />
        </a>
        <nav>
          <ul>
            <li>
              <a href="/signup" title="Sign up to Test Repo.">
                Sign Up
              </a>
            </li>
            <li><a>Docs</a></li>
            <li class="login">
              <a @click="${this.goToLogin}">Login</a>
            </li>
          </ul>
        </nav>
      </header>
      <main id="outlet"></main>
      <footer></footer>
    `;
  }

  /**
   * @desc Element is connected to DOM
   */
  connectedCallback() {
    super.connectedCallback();
    requestAnimationFrame(() => {
      this.router = new Router(this.shadowRoot.getElementById("outlet"));
      this.router.setRoutes([
        { path: "/", component: "home-view" },
        { path: "/signup", component: "signup-view" },
        { path: "/terms-conditions", component: "terms-conditions-view" },
      ]);
    });
  }
}
