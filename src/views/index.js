import {LitElement, html, css} from 'lit-element';

/**
 * @class HomeView
 * @extends LitElement
 * @desc HomeView custom element
 */
export class HomeView extends LitElement {
  /**
   * @desc Properties for the HomeView class
   * @return {Object}
   */
  static get properties() {
    return {};
  }

  /**
   * @desc Styles for the HomeView class
   * @return {Array}
   */
  static get styles() {
    return [css``];
  }

  /**
   * @desc Renders a TemplateResult to the page
   * @return {TemplateResult}
   */
  render() {
    return html`
      <article>Welcome home, world!</article>
    `;
  }
}

customElements.define('home-view', HomeView);
