import {LitElement, html, css} from 'lit-element';
import {sharedStyles} from '../css/shared.js';

/**
 * @class TermsConditionsView
 * @extends LitElement
 */
export class TermsConditionsView extends LitElement {
  /**
   * @desc Styles for the TermsConditionsView class
   * @return {Array}
   */
  static get styles() {
    return [sharedStyles, css``];
  }

  /**
   * @desc Renders a TemplateResult (html``) to the page
   * @return {TemplateResult}
   */
  render() {
    return html`
      <article>
        <h1>Terms & Conditions</h1>
      </article>
    `;
  }
}

customElements.define('terms-conditions-view', TermsConditionsView);
