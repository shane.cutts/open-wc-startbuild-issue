import {LitElement, html, css} from 'lit-element';
import {directive} from 'lit-html';
import {
  library as faLibrary,
  dom as faDom,
  config as faConfig,
} from '@fortawesome/fontawesome-svg-core';
import {
  faAngleDown,
  faCheck,
  faSpinner,
} from '@fortawesome/pro-regular-svg-icons';
import {faExclamation} from '@fortawesome/pro-solid-svg-icons';
import {sharedStyles} from '../css/shared.js';
import '../elements/product.js';

/**
 * @class SignupView
 * @desc Signup View custom element.
 * @extends LitElement
 */
export class SignupView extends LitElement {
  /**
   * @desc Constructor for SignupView
   */
  constructor() {
    super();

    const storedSignup = localStorage.getItem('testrepo-signup') || null;
    this.signupForm = storedSignup
      ? JSON.parse(storedSignup)
      : (this.signupForm = {
          FirstName: '',
          LastName: '',
          EmailAddress: '',
          ConfirmEmail: '',
          PhoneNumber: '',
          BillingName: '',
          BillingAddressLine1: '',
          BillingAddressLine2: '',
          BillingSuburb: '',
          BillingPostCode: '',
          BillingState: '',
          CGAUsername: '',
          CGAPassword: '',
          NumberOfUsers: 0,
          AgreedToTerms: false,
        });
    this.lookupResponse = '';
    this.cgaLookupIsLoading = false;
    this.formIsSubmitting = false;
    this.invalids = {
      FirstName: {invalid: false, message: ''},
      LastName: {invalid: false, message: ''},
      EmailAddress: {invalid: false, message: ''},
      ConfirmEmail: {invalid: false, message: ''},
      PhoneNumber: {invalid: false, message: ''},
      BillingName: {invalid: false, message: ''},
      BillingAddressLine1: {invalid: false, message: ''},
      BillingAddressLine2: {invalid: false, message: ''},
      BillingSuburb: {invalid: false, message: ''},
      BillingPostCode: {invalid: false, message: ''},
      BillingState: {invalid: false, message: ''},
      CGAUsername: {invalid: false, message: ''},
      CGAPassword: {invalid: false, message: ''},
      NumberOfUsers: {invalid: false, message: ''},
      AgreedToTerms: {invalid: false, message: ''},
    };

    this.prices = [
      {
        value: 99,
        usersMin: 1,
        usersMax: 50,
        tier: 'T1',
      },
      {
        value: 139,
        usersMin: 51,
        usersMax: 100,
        tier: 'T2',
      },
      {
        value: 169,
        usersMin: 101,
        usersMax: 200,
        tier: 'T3',
      },
      {
        value: 199,
        usersMin: 201,
        usersMax: 500,
        tier: 'T4',
      },
      {
        value: 'POA',
        usersMin: 501,
        tier: 'T5',
      },
    ];

    faConfig.autoAddCss = false;
    faConfig.observeMutations = true;
    faLibrary.add(faAngleDown, faCheck, faExclamation, faSpinner);
  }

  /**
   * @desc Properties for the SignupView class
   * @return {Object}
   */
  static get properties() {
    return {
      _signupForm: {type: Object},
      lookupResponse: {type: String},
      cgaLookupIsLoading: {type: Boolean},
      formIsSubmitting: {type: Boolean},
      invalids: {type: Object},
      prices: {type: Array},
      submittedSuccessfully: {type: Boolean},
    };
  }

  /**
   * @desc Setter for the signupForm property
   * @param {Object} val
   */
  set signupForm(val) {
    const oldVal = this._signupForm;
    this._signupForm = val;
    this.requestUpdate('_signupForm', oldVal);
    localStorage.setItem('testrepo-signup', JSON.stringify(this.signupForm));
  }

  /**
   * @desc Getter for signupForm property
   */
  get signupForm() {
    return this._signupForm;
  }

  /**
   * @desc Element is connected to the dom
   */
  connectedCallback() {
    super.connectedCallback();
    requestAnimationFrame(() => {
      const formElements = Array.from(
        this.shadowRoot.querySelector('form').elements
      );
      formElements.forEach((el) => {
        if (el.hasAttribute('custom-validation')) {
          let valLen = 0;
          let min = 0;
          let max = 0;
          switch (el.getAttribute('validate')) {
            case 'email':
              if (
                this.signupForm.ConfirmEmail !== this.signupForm.EmailAddress
              ) {
                el.setCustomValidity('Your email addresses must match.');
              } else {
                el.setCustomValidity('');
              }
              break;
            case 'length':
              valLen = el.value.length;
              min = el.minLength !== -1 ? el.minLength : null;
              max = el.maxLength !== -1 ? el.maxLength : null;
              if (min && el.value && valLen < min) {
                el.setCustomValidity(
                  `Please enter at least ${min} characters.`
                );
              } else if (max && el.value && valLen > max) {
                el.setCustomValidity(
                  `Please only enter up to ${max} characters.`
                );
              } else {
                el.setCustomValidity('');
              }
              break;
            default:
              break;
          }
        }
      });
    });
  }

  /**
   * @desc Fired when the element renders
   * @param {Object} changedProperties
   */
  updated(changedProperties) {
    requestAnimationFrame(() => {
      faDom.i2svg({node: this.shadowRoot});
    });
  }

  /**
   * @desc Styles for the SignupView class
   * @return {Array}
   */
  static get styles() {
    return [
      sharedStyles,
      css`
        .exciting {
          color: var(--exciting-color);
        }

        .field-group {
          display: flex;
          flex-wrap: wrap;
        }

        .form-section {
          transition: all 0.2s;
        }

        .field {
          display: flex;
          flex: 0 0 50%;
          flex-basis: calc(50% - 1rem);
          flex-direction: column;
          margin-bottom: 1.5rem;
        }

        @media (max-width: 575px) {
          .field {
            flex-basis: calc(100%);
          }
        }

        @media (min-width: 768px) {
          .number-of-users {
            flex-basis: 25%;
            flex-grow: 0;
          }

          .number-of-users,
          .products {
            align-self: flex-end;
          }
        }

        .products {
          flex-direction: row;
          align-items: stretch;
          justify-content: space-between;
          flex-grow: 1;
        }

        product-element {
          width: calc(20% - 1rem);
        }

        @media (max-width: 575px) {
          product-element {
            width: 100%;
          }

          product-element:not([selected]) {
            display: none;
          }
        }

        .cga-lookup-field {
          flex-basis: 100%;
          flex-direction: row;
          align-items: center;
        }

        @media (max-width: 575px) {
          .cga-lookup-field {
            flex-direction: column;
          }
          .cga-lookup-field button {
            margin-bottom: 1rem;
          }
        }

        .cga-lookup-field button {
          margin-right: 0.5rem;
        }

        .cga-lookup-field div {
          display: flex;
          align-items: center;
        }

        .cga-lookup-field div p {
          color: inherit;
          font-family: var(--secondary-font);
          margin: 0;
        }

        .cga-lookup-field div.lookup-success {
          color: var(--success-color);
        }

        .cga-lookup-field div.lookup-fail {
          color: var(--error-color);
        }

        svg {
          display: inline-block;
          height: 1.25em;
          text-align: center;
        }

        .cga-lookup-field div svg {
          color: inherit;
          margin-right: 0.5rem;
        }

        input {
          padding: 0.25rem 0;
        }

        select {
          padding: calc(0.25rem - 1px) 0;
        }

        h2 {
          display: flex;
          align-items: center;
          justify-content: center;
          margin-top: 0;
        }

        h2.section-heading {
          cursor: pointer;
          justify-content: space-between;
        }

        h2 svg {
          width: 1rem;
          transition: transform 0.2s;
        }

        h2[collapsed] svg {
          transform: rotate(180deg);
        }

        input,
        select {
          margin-top: 0.25rem;
          border: none;
          border-bottom: 2px solid #c4c4f0;
          transition: all 0.2s;
        }

        button:focus,
        input:focus,
        select:focus {
          outline: none;
        }

        input:focus,
        select:focus {
          border-bottom-color: #3e3ea2;
        }

        @media (min-width: 576px) {
          .field:not(:last-child) {
            margin-right: 1rem;
          }
        }

        button {
          background-color: transparent;
          width: 100%;
          max-width: 300px;
          border: 1px solid #3e3ea2;
          border-radius: 1.25rem;
          padding: 0.5rem 1rem;
          color: #3e3ea2;
          cursor: pointer;
          transition: all 0.2s;
        }

        @media (max-width: 575px) {
          button {
            max-width: none;
          }
        }

        button[disabled] {
          opacity: 0.5;
          cursor: not-allowed;
        }

        button.filled,
        button:not([disabled]):hover {
          background-color: var(--primary-color);
          color: #fff;
        }

        button.filled {
          background-color: #3e3ea2;
        }

        button.filled:not([disabled]):hover {
          background-color: transparent;
          color: #3e3ea2;
        }

        form {
          margin-top: 2rem;
        }

        .invalid-msg {
          text-align: right;
          color: var(--error-color);
          font-family: var(--secondary-font);
          font-weight: 500;
          margin-top: 0.25rem;
          margin-bottom: 0;
          font-size: 0.875rem;
        }

        input[type="checkbox"] ~ .invalid-msg {
          text-align: left;
        }

        input[touched][invalid],
        select[touched][invalid] {
          border-color: var(--error-color);
        }

        .submit {
          margin-top: 2rem;
        }

        select::-ms-expand {
          display: none;
        }

        select {
          -webkit-appearance: none;
          appearance: none;
        }

        .fa-spin {
          -webkit-animation: fa-spin 2s infinite linear;
          animation: fa-spin 2s infinite linear;
        }

        @-webkit-keyframes fa-spin {
          0% {
            -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
          }
          100% {
            -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
          }
        }

        @keyframes fa-spin {
          0% {
            -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
          }
          100% {
            -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
          }
        }

        .personal-details,
        .billing-details,
        .pricing-details {
          position: relative;
          padding-top: 1rem;
        }

        .personal-details:before,
        .billing-details:before,
        .pricing-details:before {
          display: flex;
          font-size: 1.625rem;
          font-weight: 500;
          align-items: center;
          justify-content: center;
          position: absolute;
          left: -0.5rem;
          transform: translate(-100%, 0);
          top: 1rem;
          color: var(--primary-color);
          background-color: var(--primary-light-color);
          width: 2.375rem;
          height: 2.375rem;
          border-radius: 1.1875rem;
          border: 1px solid var(--primary-color);
          z-index: 1;
          box-sizing: border-box;
          transition: all 0.2s;
        }

        form[focused="personal"] .personal-details:before {
          color: #fff;
          background-color: var(--primary-color);
        }

        form[focused="personal"] .personal-details:after {
          border-color: var(--primary-color);
        }

        form[focused="billing"] .personal-details:before,
        form[focused="billing"] .billing-details:before {
          color: #fff;
          background-color: var(--primary-color);
        }

        form[focused="billing"] .personal-details:after,
        form[focused="billing"] .billing-details:after {
          border-color: var(--primary-color);
        }

        form[focused="pricing"] .personal-details:before,
        form[focused="pricing"] .billing-details:before,
        form[focused="pricing"] .pricing-details:before {
          color: #fff;
          background-color: var(--primary-color);
        }

        form[focused="pricing"] .personal-details:after,
        form[focused="pricing"] .billing-details:after {
          border-color: var(--primary-color);
        }

        .personal-details:before {
          content: "1";
        }
        .billing-details:before {
          content: "2";
        }
        .pricing-details:before {
          content: "3";
        }

        .personal-details:after,
        .billing-details:after {
          content: "";
          border-left: 2px solid var(--primary-light-color);
          height: 100%;
          position: absolute;
          top: 1rem;
          left: -1.6875rem;
          transition: all 0.2s;
        }

        .fa-spinner {
          color: var(--primary-color);
        }

        .signup-successful {
          color: var(--accent-green-color);
          text-align: center;
          margin-top: 2rem;
        }

        .signup-successful h2 {
          color: inherit;
          font-weight: 500;
        }

        .signup-successful svg {
          color: inherit;
          margin-right: 2rem;
          width: 1.75rem;
        }

        @media (max-width: 1260px) {
          form {
            padding: 0 2rem 0 3rem;
          }
        }
      `,
    ];
  }

  /**
   * @desc Renders a TemplateResult (html``) to the page
   * @return {TemplateResult}
   */
  render() {
    const getInvalidMsg = directive((prop) => (part) => {
      const {invalid} = this.invalids[prop];
      const invalidMsg = this.invalids[prop].message;
      part.setValue(
        invalid
          ? html`
              <p class="invalid-msg">${invalidMsg}</p>
            `
          : null
      );
    });

    const focusSection = {
      handleEvent(event) {
        const section = event.target.closest('section');
        if (section) {
          event.currentTarget.setAttribute('focused', section.dataset.section);
        }
      },
      capture: true,
    };

    const blurSection = {
      handleEvent(event) {
        event.currentTarget.removeAttribute('focused');
      },
      capture: true,
    };

    const footerContent = () => {
      if (this.submittedSuccessfully) {
        return html`
          <div class="signup-successful">
            <h2><i class="far fa-check"></i> Sign Up Complete!</h2>
            <p>
              Congratulations, you are now signed up for Test Repo. You will
              receive an email confirming your sign up details.
            </p>
          </div>
        `;
      }
      return html`
        <button
          ?disabled=${this.formIsSubmitting}
          type="submit"
          class="submit filled"
        >
          Sign Up
        </button>
      `;
    };

    return html`
      <article>
        <h1>Sign Up</h1>
        <p>
          A comprehensive suite of tools providing efficient management
          analysis and reporting of your BroadWorks platform.
          <span class="exciting">First month <strong>FREE!</strong></span>
        </p>
        <form
          @input=${this.inputChange}
          @submit=${this.submitForm}
          @focus=${focusSection}
          @blur=${blurSection}
          novalidate
        >
          <section data-section="personal" class="personal-details">
            <h2 class="section-heading" @click=${this.toggleContent}>
              Personal Details
              <i class="far fa-angle-down"></i>
            </h2>
            <div class="form-section">
              <div class="field-group">
                <div class="field">
                  <label for="FirstName">First Name</label>
                  <input
                    id="FirstName"
                    name="First name"
                    type="text"
                    placeholder="Enter your first name"
                    required
                    .value=${this.signupForm.FirstName}
                    @blur=${this.blurInput}
                  />
                  ${getInvalidMsg('FirstName')}
                </div>
                <div class="field">
                  <label for="LastName">Last Name</label>
                  <input
                    id="LastName"
                    name="Last name"
                    type="text"
                    placeholder="Enter your last name"
                    required
                    .value=${this.signupForm.LastName}
                    @blur=${this.blurInput}
                  />
                  ${getInvalidMsg('LastName')}
                </div>
                <div class="field">
                  <label for="EmailAddress">Email</label>
                  <input
                    id="EmailAddress"
                    name="Email"
                    type="email"
                    placeholder="Enter your email address"
                    required
                    custom-validation
                    validate="email"
                    .value=${this.signupForm.EmailAddress}
                    @blur=${this.blurInput}
                  />
                  ${getInvalidMsg('EmailAddress')}
                </div>
                <div class="field">
                  <label for="ConfirmEmail">Confirm Email</label>
                  <input
                    id="ConfirmEmail"
                    name="Confirm email"
                    type="email"
                    placeholder="Confirm your email address"
                    required
                    custom-validation
                    validate="email"
                    .value=${this.signupForm.ConfirmEmail}
                    @blur=${this.blurInput}
                  />
                  ${getInvalidMsg('ConfirmEmail')}
                </div>
                <div class="field">
                  <label for="PhoneNumber">Phone Number</label>
                  <input
                    id="PhoneNumber"
                    name="Phone number"
                    type="text"
                    placeholder="Enter your phone number"
                    minlength="8"
                    custom-validation
                    validate="length"
                    required
                    .value=${this.signupForm.PhoneNumber}
                    @blur=${this.blurInput}
                  />
                  ${getInvalidMsg('PhoneNumber')}
                </div>
              </div>
            </div>
          </section>
          <section data-section="billing" class="billing-details">
            <h2 class="section-heading" @click=${this.toggleContent}>
              Billing Details
              <i class="far fa-angle-down"></i>
            </h2>
            <div class="form-section">
              <div class="field-group">
                <div class="field">
                  <label for="BillingName">Billing Name</label>
                  <input
                    id="BillingName"
                    name="Billing name"
                    type="text"
                    placeholder="Enter a billing name"
                    required
                    .value=${this.signupForm.BillingName}
                    @blur=${this.blurInput}
                  />
                  ${getInvalidMsg('BillingName')}
                </div>
                <div class="field">
                  <label for="BillingAddressLine1">Address Line 1</label>
                  <input
                    id="BillingAddressLine1"
                    name="Address line 1"
                    type="text"
                    placeholder="e.g. Unit 1 - Building name"
                    required
                    .value=${this.signupForm.BillingAddressLine1}
                    @blur=${this.blurInput}
                  />
                  ${getInvalidMsg('BillingAddressLine1')}
                </div>
                <div class="field">
                  <label for="BillingAddressLine2"
                    >Address Line 2 (optional)</label
                  >
                  <input
                    id="BillingAddressLine2"
                    name="Address line 2"
                    type="text"
                    placeholder="e.g. 10 Street Name Rd"
                    .value=${this.signupForm.BillingAddressLine2}
                    @blur=${this.blurInput}
                  />
                  ${getInvalidMsg('BillingAddressLine2')}
                </div>
                <div class="field">
                  <label for="BillingSuburb">Suburb</label>
                  <input
                    id="BillingSuburb"
                    name="Suburb"
                    type="text"
                    placeholder="Enter your suburb"
                    required
                    .value=${this.signupForm.BillingSuburb}
                    @blur=${this.blurInput}
                  />
                  ${getInvalidMsg('BillingSuburb')}
                </div>
                <div class="field">
                  <label for="BillingPostCode">Post Code</label>
                  <input
                    id="BillingPostCode"
                    name="Post code"
                    type="text"
                    minlength="4"
                    maxlength="4"
                    pattern="[0-9]*"
                    custom-validation
                    validate="length"
                    placeholder="Enter your post code"
                    required
                    .value=${this.signupForm.BillingPostCode}
                    @blur=${this.blurInput}
                  />
                  ${getInvalidMsg('BillingPostCode')}
                </div>
                <div class="field">
                  <label for="BillingState">State</label>
                  <select
                    id="BillingState"
                    name="State"
                    type="text"
                    placeholder="Select your state"
                    required
                    value=${this.signupForm.BillingState}
                    @blur=${this.blurInput}
                  >
                    <option value="ACT">ACT</option>
                    <option value="NSW">NSW</option>
                    <option value="NT">NT</option>
                    <option value="QLD">QLD</option>
                    <option value="SA">SA</option>
                    <option value="TAS">TAS</option>
                    <option value="VIC">VIC</option>
                    <option value="WA">WA</option>
                  </select>
                  ${getInvalidMsg('BillingState')}
                </div>
              </div>
            </div>
          </section>
          <section data-section="pricing" class="pricing-details">
            <h2 class="section-heading" @click=${
              this.toggleContent
            }>Pricing <i class="far fa-angle-down"></i></h2>
            <div class="form-section">
              <p>
                Select the Test Repo plan you would like to move to
                after your free month. You can also optionally enter your
                BroadWorks login details; we will then lookup your current user
                limit and select the most suitable plan for you.
              </p>
              <h3>CommPilot Login</h3>
              <div class="field-group">
                <div class="field">
                  <label for="CGAUsername">
                    CGA (Customer Group Administrator) Username
                  </label>
                  <input
                    id="CGAUsername"
                    name="CGA Username"
                    type="text"
                    placeholder="Enter your CGA username"
                    required
                    .value=${this.signupForm.CGAUsername}
                    @blur=${this.blurInput}
                  />
                  ${getInvalidMsg('CGAUsername')}
                </div>
                <div class="field">
                  <label for="CGAPassword">CGA Password</label>
                  <input
                    id="CGAPassword"
                    name="CGA Password"
                    type="password"
                    placeholder="Enter your CGA password"
                    required
                    .value=${this.signupForm.CGAPassword}
                    @blur=${this.blurInput}
                  />
                  ${getInvalidMsg('CGAPassword')}
                </div>
                <div class="field cga-lookup-field">
                  <button
                    ?disabled=${this.cgaLookupIsLoading}
                    type="button"
                    class="lookupUserTotal"
                  >
                    Lookup User Total
                  </button>
                  ${
                    this.cgaLookupIsLoading
                      ? html`
                          <i class="far fa-spinner fa-spin"></i>
                        `
                      : this.lookupResponse
                  }
                </div>
              </div>
              <h3>Pricing Summary</h3>
              <div class="field-group">
                <div class="field number-of-users">
                  <label for="NumberOfUsers">Number of Users</label>
                  <input
                    id="NumberOfUsers"
                    name="Number of Users"
                    type="number"
                    placeholder="Enter number of users"
                    min="0"
                    max="2000"
                    required
                    .value=${this.signupForm.NumberOfUsers}
                    @input=${this.numberOfUsersInput}
                    @blur=${this.blurInput}
                  />
                  ${getInvalidMsg('NumberOfUsers')}
                </div>
                <div class="field products">
                  ${this.prices.map(
                    (price) => html`
                      <product-element
                        .numberOfUsers="
                        ${this.signupForm.NumberOfUsers}"
                        .min=${price.usersMin}
                        .max=${price.usersMax}
                        .value=${price.value}
                        .tier=${price.tier}
                      ></product-element>
                    `
                  )}
                </div>
              </div>
              <p>
                <span class="">*</span> The number of users is calculated
                based on the total amount of users you have in your BroadWorks
                Enterprise/Group. This is regardless of any service, device
                profile, phone number, etc. assigned to each user.
              </p>
              <div class="field">
                <input
                  required
                  id="AgreedToTerms"
                  type="checkbox"
                  name="Agreed to terms"
                  value="Yes"
                  .checked=${this.signupForm.AgreedToTerms}
                />
                <label class="checkbox" for="AgreedToTerms">
                  <span class="selector">
                    <i class="far fa-check"></i>
                  </span>
                  <span
                    >I agree to the
                    <a
                      href="/terms-conditions"
                      title="View Test Repo signup terms and conditions"
                    >
                      terms and conditions </a
                    >.
                  </span>
                </label>
                ${getInvalidMsg('AgreedToTerms')}
              </div>
            </section>
            ${footerContent()}
        </form>
      </article>
    `;
  }

  /**
   * @desc Toggle content seen for section
   * @param {Event} event
   */
  toggleContent(event) {
    const h2 = event.currentTarget;
    const content = h2.nextElementSibling;
    if (h2.hasAttribute('collapsed')) {
      h2.removeAttribute('collapsed');
      content.style.height = `${0}px`;
      const transitionEnd = () => {
        content.style.height = 'auto';
        content.style.overflow = 'visible';
        content.removeEventListener('transitionend', transitionEnd);
      };
      requestAnimationFrame(() => {
        content.style.height = `${content.scrollHeight}px`;
        content.addEventListener('transitionend', transitionEnd);
      });
    } else {
      h2.setAttribute('collapsed', '');
      content.style.height = `${content.scrollHeight}px`;
      content.style.overflow = 'hidden';
      requestAnimationFrame(() => {
        content.style.height = `${0}px`;
      });
    }
  }

  /**
   * @desc Number of users is updated
   * @param {Event} event
   */
  numberOfUsersInput(event) {
    const val = event.target.value;
    event.target.value = val ? parseInt(val) : val;
  }

  /**
   * @desc Get the HTMLElement
   * @param {String} query
   * @return {HTMLElement}
   */
  el(query) {
    return this.shadowRoot.querySelector(query);
  }

  /**
   * @desc Fire on input blur to set touched attribute
   * @param {Event} event
   */
  blurInput(event) {
    this.checkValidity(event.target);
  }

  /**
   * @edsc Fired from <form> on input of a child element
   * @param {Event} event
   */
  inputChange(event) {
    const prop = event.target.id;
    if (event.target.type === 'checkbox') {
      this.signupForm = {
        ...this.signupForm,
        [prop]: event.target.checked,
      };
    } else {
      this.signupForm = {
        ...this.signupForm,
        [prop]: event.target.value,
      };
    }
    this.checkValidity(event.target);
  }

  /**
   * @desc Check FormElement validity base on JS Constraint Validation API
   * @param {HTMLElement} el
   */
  checkValidity(el) {
    if (!el.hasAttribute('touched')) {
      el.setAttribute('touched', '');
    }
    const prop = el.id;
    const invalid = !el.checkValidity();
    const invalidMsg = el.validationMessage;
    if (invalid) {
      el.setAttribute('invalid', '');
    } else {
      el.removeAttribute('invalid');
    }
    this.invalids = {
      ...this.invalids,
      [prop]: {...this.invalids[prop], invalid, message: invalidMsg},
    };
  }

  /**
   * @desc Submit the form
   * @param {Event} event
   */
  submitForm(event) {
    event.preventDefault();
    if (this.formIsSubmitting) return;
    if (!event.target.checkValidity()) {
      const elements = Array.from(event.target.elements);
      elements.forEach((element, index) => {
        this.checkValidity(element);
      });
      window.scrollTo({
        top: this.shadowRoot.querySelector('[touched][invalid]').offsetTop - 50,
        behavior: 'smooth',
      });
    } else {
      this.formIsSubmitting = true;
      fetch('someaddress', {
        method: 'POST',
        mode: 'cors',
        headers: {
          'Content-type': 'application/json; charset=utf-8',
        },
        body: JSON.stringify({signupInfo: this.signupForm}),
      })
        .then((resp) => {
          console.log(resp);
          this.formIsSubmitting = false;
          this.onSubmitSuccess();
        })
        .catch((err) => {
          console.log(err);
          this.formIsSubmitting = false;
        });
    }
  }

  /**
   * @desc Do something on submit success
   */
  onSubmitSuccess() {
    const headings = this.shadowRoot.querySelectorAll('h2');
    for (const heading of headings) {
      heading.click();
    }
    this.submittedSuccessfully = true;
    this.shadowRoot.querySelector('form').setAttribute('focused', 'pricing');
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
    if (localStorage.getItem('testrepo-signup')) {
      localStorage.removeItem('testrepo-signup');
    }
  }
}

customElements.define('signup-view', SignupView);
