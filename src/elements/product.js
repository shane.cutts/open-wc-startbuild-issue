import { LitElement, html, css } from "lit-element";
import { sharedVars } from "../css/variables.js";
/**
 * @class ProductElement
 * @extends LitElement
 * @desc ProductElement custom element
 */
export class ProductElement extends LitElement {
  constructor() {
    super();
    this.value = "";
    this.tier = "";
    this.min = 0;
    this.max = 0;
    this.numberOfUsers = 0;
  }

  /**
   * @desc Properties for the ProductElement class
   * @return {Object}
   */
  static get properties() {
    return {
      value: { type: Number },
      tier: { type: String },
      min: { type: Number },
      max: { type: Number },
      numberOfUsers: { type: Number },
    };
  }

  updated(changedProperties) {
    changedProperties.forEach((oldVal, propName) => {
      if (propName === "numberOfUsers") {
        if (this[propName] >= this.min && (this[propName] <= this.max || !this.max)) {
          this.setAttribute('selected', '');
        } else {
          this.removeAttribute('selected');
        }
      }
    });
  }

  /**
   * @desc Styles for the ProductElement class
   * @return {Array}
   */
  static get styles() {
    return [
      sharedVars,
      css`
        :host {
          display: flex;
          flex-direction: column;
          flex-shrink: 0;
          flex-grow: 0;
          text-align: center;
          padding: 0.25rem;
        }

        :host([selected]) .value-wrapper,
        :host([selected]) .tier {
          background-color: var(--primary-light-color);
        }

        .product-wrapper {
          display: flex;
          flex-direction: column;
          flex: 1;
          align-items: stretch;
          justify-content: center;
          padding: 0.25rem;
          border: 1px solid var(--primary-color);
          transition: background-color 0.2s;
        }

        .number-of-users {
          background-color: var(--primary-color);
          color: #ffffff;
          opacity: 0;
          transition: opacity 0.2s;
        }

        :host([selected]) .number-of-users {
          opacity: 1;
        }

        .value-wrapper {
          font-size: 1.25rem;
          color: var(--primary-color);
        }

        .value {
          font-size: 1.5rem;
          font-weight: 500;
        }

        span:not(.value) {
          font-size: 1rem;
        }

        :host([selected]) .tier {
          color: #ffffff;
        }

        .tier {
          font-size: 2rem;
          font-weight: 500;
          color: rgba(64, 64, 161, 0.35);
          transition: color 0.2s;
        }
      `,
    ];
  }

  /**
   * @desc Renders a TemplateResult to the page
   * @return {TemplateResult}
   */
  render() {
    return html`
      <div class="number-of-users">${this.numberOfUsers} Users</div>
      <div class="product-wrapper">
        <div class="value-wrapper">
          $<span class="value">${this.value}</span>${typeof this.value ===
          "string"
            ? null
            : html`
                <span>/mth</span>
              `}
        </div>
        <div class="tier">${this.tier}</div>
      </div>
    `;
  }
}

customElements.define("product-element", ProductElement);
