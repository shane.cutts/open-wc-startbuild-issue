import {css} from 'lit-element';

export const sharedVars = css`
  /*** CUSTOM PROPERTIES ***/
  :host {
    --primary-color: #4040a1;
    --secondary-color: #17173a;
    --primary-light-color: #ddddf6;
    --accent-green-color: #00c2af;
    --success-color: #00d46e;
    --error-color: #ff0000;
    --exciting-color: #ff6b00;
    --error-color: #ff4446;
    --link-color: #5454d4;

    --primary-font: "Montserrat", sans-serif;
    --secondary-font: "Roboto", sans-serif;
  }
`;
