import {css} from 'lit-element';
import {sharedVars} from './variables.js';

export const sharedStyles = [
         sharedVars,
         css`
           h1,
           h2,
           h3,
           h4,
           h5,
           h6,
           p,
           li,
           a,
           span {
             font-family: var(--primary-font);
             font-weight: 400;
             color: var(--secondary-color);
           }

           h1 {
             color: var(--primary-color);
             font-size: 2.25rem;
             font-weight: 500;
             margin-bottom: 1rem;
           }

           h2 {
             font-size: 1.75rem;
             color: var(--secondary-color);
           }

           label,
           input,
           select,
           button {
             font-family: var(--secondary-font);
             font-size: 1rem;
           }

           p,
           label,
           input,
           textarea,
           select {
             line-height: 1.5rem;
           }

           label {
             font-size: 0.9375rem;
             color: #777c7b;
           }

           a {
             color: var(--link-color);
           }

           input[type="checkbox"] {
             opacity: 0;
             position: absolute;
             z-index: -1;
           }

           .checkbox {
             display: flex;
             align-items: center;
             position: relative;
           }

           .checkbox .selector {
             display: flex;
             position: relative;
             width: 1.25rem;
             height: 1.25rem;
             border-radius: 1rem;
             border: 1px solid var(--secondary-color);
             margin-right: 0.5rem;
           }

           .checkbox .selector svg {
             width: 1.45rem;
             height: 1.45rem;
             position: absolute;
             top: 35%;
             left: 65%;
             transform: translate(-50%, -50%);
             opacity: 0;
             transition: all 0.2s;
           }

           input[type="checkbox"]:checked + .checkbox .selector svg {
             opacity: 1;
           }
         `,
       ];
