// import { createDefaultConfig } from '@open-wc/building-rollup';

// export default createDefaultConfig({ input: './index.html' });

// if you need to support IE11 use "modern-and-legacy-config" instead.
import {createCompatibilityConfig} from '@open-wc/building-rollup';

const configs = createCompatibilityConfig({input: './index.html'});

export default configs.map(config => ({
  ...config,
  output: {
    ...config.output,
  },
}));
